### Image Details ###

* Ubuntu 20.04 (LTS)
* Dotnet-sdk-6.0
* Dotnet-runtime-6.0
* PowerShell 7.2.1
* Docker 20.10.12

### Build Image ###
docker build -f ubuntu-docker-dotnet-pwsh.dockerfile -t ubuntu-docker-dotnet-pwsh .

### Build Container - Starts up root terminal ###
docker run -i -t ubuntu-docker-dotnet-pwsh

### Close Container ###
Type 'exit' to close