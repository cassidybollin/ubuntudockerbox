FROM ubuntu

RUN apt-get update

RUN apt-get install -y\
    ca-certificates \
    curl \
    sudo \
    gnupg \
    lsb-release
RUN apt-get install wget

#Installing Microsoft package signing key
RUN wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb 
RUN sudo dpkg -i packages-microsoft-prod.deb 
RUN rm packages-microsoft-prod.deb

#Installing dotnet SDK
RUN sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y dotnet-sdk-6.0

#Installing aspnetcore RUNTIME
RUN apt-get update 
RUN sudo apt-get install -y apt-transport-https
RUN sudo apt-get update
RUN sudo apt-get install -y aspnetcore-runtime-6.0

# Installing Powershell
RUN sudo apt-get update
RUN sudo apt-get install -y wget apt-transport-https software-properties-common
RUN wget -q https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb
RUN sudo dpkg -i packages-microsoft-prod.deb
RUN sudo apt-get update
RUN sudo apt-get install -y powershell
RUN pwsh

# Set up the Docker repo
RUN sudo apt-get update
RUN sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
RUN echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

RUN sudo apt-get update
RUN sudo apt-get install docker-ce docker-ce-cli containerd.io -y
