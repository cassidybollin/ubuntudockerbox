#!groovy
@Library('amc')
import com.amctheatres.PipelineHelpers
import com.amctheatres.dtr.DtrScanCompleteWebhook
import hudson.Util;
import groovy.json.JsonOutput;

def hook = new DtrScanCompleteWebhook(this, 'UbuntuDockerBox')
def STAGE = 'Initializing'
def repoHost = 'artifactory.amctheatres.com'
def dockerRepo = 'consumer-docker'
def dockerImageName = 'mcr.microsoft.com/dotnet/core/sdk:2.2-alpine3.9'
def agentLabel = 'linux'
def packageName = 'UbuntuDockerBox'
def packageVersion
def artifactoryServer
def rtDocker
def dtrDomain = 'dtr.perimeter.amc.corp'

def containerHook = new DtrScanCompleteWebhook(this, 'UbuntuDockerBox')

pipeline {
    agent any
     environment {
        HOME = '/tmp'
        NUGET_PACKAGES = '/tmp'
        TZ = 'US/Central'
        APP_NAME = 'UbuntuDockerBox'
        ARTIFACTORY= credentials('artifactory')
    }

    options {
        timestamps()
        buildDiscarder logRotator(numToKeepStr: '2')
    }
    
    parameters {
        booleanParam(name: 'BuildContainer', defaultValue: true, description: 'Build Ubuntu Container')
    }
    triggers {
        cron(env.BRANCH_NAME == 'develop' ? 'H 3 * * *': '')
        pollSCM('H/15 * * * *')
    }
    stages {
        stage('Build Test') {
            agent {
                label agentLabel
            }
            when { equals expected: true, actual: params.Dockerize }
            steps {
                script {
                    versionString = GetReleaseVersionLinux()

                        STAGE = env.STAGE_NAME

                        packageVersion = env.BUILD_ID

                        if  (!packageVersion) {
                            error "Could not determine package version"
                        }
                        sh 'docker system prune -f'

                        currentBuild.description = packageVersion

                        branchName = env.GIT_BRANCH
                     
                        artifactoryServer = Artifactory.server 'artifactory'
                        rtDocker = Artifactory.docker server: artifactoryServer

                        docker.withRegistry("https://${repoHost}/${dockerRepo}", 'artifactory') {
							def imageName = "${repoHost}/${dockerRepo}/${packageName}:${packageVersion}"
                            def builtImage = docker.build(imageName, "--build-arg ARTIFACTORY_USR --build-arg ARTIFACTORY_PSW -f ./UbuntuDockerBox/Dockerfile .")
                            def buildInfo = rtDocker.push imageName, dockerRepo
                            artifactoryServer.publishBuildInfo buildInfo
                        }
                    }
            }
        }
        stage('Build Docker image') {
            agent {
                label agentLabel
            }
            when { equals expected: true, actual: params.Dockerize }
            steps {
                script {
                    versionString = GetReleaseVersionLinux()
                        STAGE = env.STAGE_NAME

                        packageVersion = env.BUILD_ID

                        if  (!packageVersion) {
                            error "Could not determine package version"
                        }
                        sh 'docker system prune -f'

                        currentBuild.description = packageVersion

                        branchName = env.GIT_BRANCH
                     
                        artifactoryServer = Artifactory.server 'artifactory'
                        rtDocker = Artifactory.docker server: artifactoryServer

                        docker.withRegistry("https://${repoHost}/${dockerRepo}", 'artifactory') {
							def imageName = "${repoHost}/${dockerRepo}/${packageName}:${packageVersion}"
                            def builtImage = docker.build(imageName, "--build-arg ARTIFACTORY_USR --build-arg ARTIFACTORY_PSW -f ./ubuntu-docker-dotnet-pwsh/Dockerfile .")
                            def buildInfo = rtDocker.push imageName, dockerRepo
                            artifactoryServer.publishBuildInfo buildInfo
                        }
                    }
            }
        }
    }
    post {
        always {
            script {
                if (params.Test) {
                    mstest testResultsFile:"**/Results*.xml", keepLongStdio: true, failOnError: false
                }
                
                SendSlackMessage()
            }
        }
        success {
            cleanWs()
        }
        changed {
            script {
                if (currentBuild.currentResult == 'SUCCESS') {
                    emailext subject: '$DEFAULT_SUBJECT',
                             body: '$DEFAULT_CONTENT',
                             recipientProviders: [[$class: 'CulpritsRecipientProvider'], [$class: 'RequesterRecipientProvider']],
                             to: env.BRANCH_NAME == 'develop' ? '$DEFAULT_RECIPIENTS' : ''
                }
            }
        }
        failure {
            emailext subject: '$DEFAULT_SUBJECT',
                     body: '$DEFAULT_CONTENT\n\n${CHANGES, format="[%a]: %m"}\n${BUILD_LOG, escapeHtml=false}',
                     recipientProviders: [[$class: 'CulpritsRecipientProvider'], [$class: 'RequesterRecipientProvider']],
                     to: env.BRANCH_NAME == 'develop' ? '$DEFAULT_RECIPIENTS' : ''
        }
    }
}

def SendSlackMessage() {
    colorCode = currentBuild.currentResult == 'SUCCESS' ? 'good' : 'danger'
    slackSend (color: colorCode, message: "$currentBuild.fullDisplayName - $currentBuild.currentResult (<$BUILD_URL|Open>)")
}

def GetReleaseVersion() {
    commitId = powershell(returnStdout: true, script: 'git rev-parse HEAD')
    timeStamp = powershell(returnStdout: true, script: 'git show -s --format=%ct $commitId')
    
    commitId = commitId.substring(0, 7)
    timeStamp = timeStamp.trim()
    
    currentDate = new Date(Long.valueOf(timeStamp) * 1000)
    dateString = currentDate.format("yyyy.MM.dd.HHmm")
    
    return "$dateString-$env.BRANCH_NAME-$commitId"
}

def GetReleaseVersionLinux() {
    commitId = sh(returnStdout: true, script: 'git rev-parse HEAD').trim()
    timeStamp = sh(returnStdout: true, script: 'git show -s --format=%ct $commitId').trim()
    
    commitId = commitId.substring(0, 7)
    timeStamp = timeStamp.trim()
    
    currentDate = new Date(Long.valueOf(timeStamp) * 1000)
    dateString = currentDate.format("yyyy.MM.dd.HHmm")
    
    return "$dateString-$env.BRANCH_NAME-$commitId"
}